/**
 * @file
 * Main library for the uw_studentbudget_calculator module.
 */

/**
 * Empty static class wrapper.
 */
function uw_studentbudget_calculator() {
};

/**
 * Selects the text in a input field specified by parameter id.
 *
 * @param id
 *   ID of the input field to select and put focus on
 */
uw_studentbudget_calculator.selectContents = function (id) {
  try {
    document.getElementById(id).focus();
    document.getElementById(id).select();
  }
  catch (e) {
    // Doesn't matter
    // debug.
    alert(e);
  }
}

/**
 * Returns an integer representation of the value read from a input field.
 *
 * Returns 0 if no valid value can be read.
 */
uw_studentbudget_calculator.getFloatValue = function (id) {
  try {
    var element = document.getElementById(id);
    var value = parseFloat(element.value);
    if (isNaN(value)) {
      element.value = 0;
      return 0;
    }
    else {
      var returnItem = value.toFixed(2);
      if (returnItem.length < 9) {
        element.value = returnItem;
        return value;
      }
      else {
        element.value = 0;
        return 0.00;
      }
    }
  }
  catch (e) {
    return 0;
  }
}

/**
 * Calculates the two values (amount * month) and sets the totalId holder with that information.
 *
 * @return
 *   Calculated value.
 */
uw_studentbudget_calculator.setCalculatedValue = function (totalId, amountId, monthId) {
  var calculatedValue = (uw_studentbudget_calculator.getFloatValue(amountId) * uw_studentbudget_calculator.getFloatValue(monthId));
  document.getElementById(totalId).innerHTML = '$' + calculatedValue.toFixed(2);
  return calculatedValue;
}

/**
 * Calculate the value from the forms on the fly.
 */
uw_studentbudget_calculator.calculate = function () {
  try {
    // Resources total.
    var totalResources = (uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-beginbalance")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-parents")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-awards")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-parttime")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-government-orphan")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-government-welfare")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-osap-canada")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-otherincome-actions-othervalue")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-otherincome-actions-other2value")
                          );
    document.getElementById("uw_studentbudget_calculator_totalresources_calculated_value").innerHTML = "$" + totalResources.toFixed(2);

    // Expenses total.
    var totalExpenses = (
                          uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-tuition")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-books")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-residence")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-residencephone")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusrent")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusutilities")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusphone")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusgroceries")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-personalexpenses")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-clothing")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-personallaundry")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-personalent")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-transportation-local")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-transportation-home")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-otherexpenses-actions-othervalue")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-otherexpenses-actions-other2value")

                        );
    document.getElementById("uw_studentbudget_calculator_totalexpenses_calculated_value").innerHTML = "$" + totalExpenses.toFixed(2);

    // Financial need.
    document.getElementById("uw_studentbudget_calculator_netresources_calculated_value").innerHTML = "$" + (totalResources - totalExpenses).toFixed(2);
  }
  catch (e) {
    // error
    // ignore.
  }
}

/**
 * Reset form.
 */
uw_studentbudget_calculator.reset = function () {

  try {
    var balance = 0;
    var parents = 0;
    var awards = 0;
    var parttime = 0;
    var orphan = 0;
    var welfare = 0;
    var canada = 0;
    var othervalue = 0;
    var other2value = 0;
    var tuition = 0;
    var books = 0;
    var residence = 0;
    var residencephone = 0;
    var offcampusrent = 0;
    var offcampusutilities = 0;
    var offcampusphone = 0;
    var offcampusgroceries = 0;
    var personalexpenses = 0;
    var clothing = 0;
    var personallaundry = 0;
    var personalent = 0;
    var local = 0;
    var home = 0;
    var totalResources = 0;
    var totalExpenses = 0;

    // Set three total values to 0.00.
    document.getElementById("uw_studentbudget_calculator_totalresources_calculated_value").innerHTML = "$" + totalResources.toFixed(2);
    document.getElementById("uw_studentbudget_calculator_totalexpenses_calculated_value").innerHTML = "$" + totalExpenses.toFixed(2);
    document.getElementById("uw_studentbudget_calculator_netresources_calculated_value").innerHTML = "$" + (totalResources - totalExpenses).toFixed(2);

    // Set fields of "Your resources" value to 0.00.
    document.getElementById("edit-uw-studentbudget-calculator-beginbalance").value = balance.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-parents").value = parents.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-awards").value = awards.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-parttime").value = parttime.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-government-orphan").value = orphan.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-government-welfare").value = welfare.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-osap-canada").value = canada.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-otherincome-actions-othervalue").value = othervalue.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-otherincome-actions-other2value").value = other2value.toFixed(2);

    // Set fields of "Your expenses" value to 0.00.
    document.getElementById("edit-uw-studentbudget-calculator-tuition").value = tuition.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-books").value = books.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-residence").value = residence.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-residencephone").value = residencephone.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-offcampusrent").value = offcampusrent.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-offcampusutilities").value = offcampusutilities.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-offcampusphone").value = offcampusphone.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-offcampusgroceries").value = offcampusgroceries.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-personalexpenses").value = personalexpenses.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-clothing").value = clothing.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-personallaundry").value = personallaundry.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-personalent").value = personalent.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-transportation-local").value = local.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-transportation-home").value = home.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-otherexpenses-actions-othervalue").value = othervalue.toFixed(2);
    document.getElementById("edit-uw-studentbudget-calculator-otherexpenses-actions-other2value").value = other2value.toFixed(2);

    // Set description of items to empty.
    document.getElementById("edit-uw-studentbudget-calculator-otherincome-actions-otherdesc").value = "Description";
    document.getElementById("edit-uw-studentbudget-calculator-otherincome-actions-other2desc").value = "Description";
    document.getElementById("edit-uw-studentbudget-calculator-otherexpenses-actions-otherdesc").value = "Description";
    document.getElementById("edit-uw-studentbudget-calculator-otherexpenses-actions-other2desc").value = "Description";
  }
  catch (e) {
    // error
    // ignore.
  }
}
