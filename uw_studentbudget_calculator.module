<?php

/**
 * @file
 * Student budget calculator module for UW.
 */

/**
 * Implements hook_menu().
 */
function uw_studentbudget_calculator_menu() {
  $items = array();

  $items['uw_studentbudget_calculator'] = array(
    'title'            => 'Interactive Calculator',
    'page callback'    => 'drupal_get_form',
    'page arguments'  => array('uw_studentbudget_calculator_displayform'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => "main-menu",
    'weight' => 50,
  );

  return $items;
}

/**
 * Quick method for retreiving session value or substituting a default.
 *
 * @param string $name
 *   Session Key.
 * @param string $default_value
 *   If nothing exists in the session yet.
 * @param bool $return_default
 *   If TRUE, it only returns the default value.
 *
 * @return string
 *   Value stored in the session or the default value.
 */
function uw_studentbudget_calculator_retreivesession_default($name, $default_value, $return_default = TRUE) {
  if ($return_default == TRUE || !isset($_SESSION['uw_studentbudget_calculator_data'][$name])) {
    return $default_value;
  }
  else {
    return $_SESSION['uw_studentbudget_calculator_data'][$name];
  }
}

/**
 * Creates the calculation form.
 *
 * @param $form
 *   The form to modify.
 *
 * @return array
 *   The form array.
 */
function uw_studentbudget_calculator_generateform($form) {
  $path = drupal_get_path('module', 'uw_studentbudget_calculator');
  drupal_add_js($path . '/js/uw_studentbudget_calculator.js');
  drupal_add_css($path . '/theme/css/uw_studentbudget_calculator.css');

  $return_default = !isset($_GET['c']);

  $form['uw_studentbudget_calculator_study_period_heading'] = array(
    '#markup' => t('<strong>The following calculation is based on the selected study period. </strong>'),
  );

  $form['uw_studentbudget_calculator_study_period'] = array(
    '#type' => 'radios',
    '#options' => array(
      0 => t('Jan-April'),
      1 => t('May-Aug'),
      2 => t('Sept-Dec'),
      3 => t('Jan-Aug'),
      4 => t('Sept-April'),
    ),
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('study_period', '0', $return_default),
  );

  $form['uw_studentbudget_calculator_resources_heading'] = array(
    '#prefix' => '<div class="uw_studentbudget_calculator_mainsection">',
    '#markup' => t('<h2>Your resources</h2>'),
  );

  $form['uw_studentbudget_calculator_personalincome_heading'] = array(
    '#markup' => t('<h3>Personal income</h3>'),
  );

  $form['uw_studentbudget_calculator_beginbalance'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank balance'),
    '#field_prefix' => t('Your projected bank balance at the start of school'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('beginbalance', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-beginbalance')",
    ),
  );

  $form['uw_studentbudget_calculator_parents'] = array(
    '#type' => 'textfield',
    '#title' => t('Registered Education Savings Plan'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('parents', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-parents')",
    ),
  );

  $form['uw_studentbudget_calculator_awards'] = array(
    '#type' => 'textfield',
    '#title' => t('Scholarships & awards'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('awards', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-awards')",
    ),
  );

  $form['uw_studentbudget_calculator_parttime'] = array(
    '#type' => 'textfield',
    '#title' => t('Part-time work during school'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('parttime', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-parttime')",
    ),
  );

  $form['uw_studentbudget_calculator_governmentincome_heading'] = array(
    '#markup' => t('<h3>Government income</h3>'),
  );

  $form['uw_studentbudget_calculator_government_orphan'] = array(
    '#type' => 'textfield',
    '#title' => t('Canada Pension Plan'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('government_orphan', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-government-orphan')",
    ),
  );

  $form['uw_studentbudget_calculator_government_welfare'] = array(
    '#type' => 'textfield',
    '#title' => t('Ontario Works'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('government_welfare', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-government-welfare')",
    ),
  );

  $form['uw_studentbudget_calculator_osap_canada'] = array(
    '#type' => 'textfield',
    '#title' => t('OSAP assistance estimate'),
    '#field_prefix' => t('Include all loans and grants including 30% off grant'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('osap_canada', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-osap-canada')",
    ),
  );

  $form['uw_studentbudget_calculator_otherincome_heading'] = array(
    '#markup' => t('<h3>Other income (specify)</h3>'),
    '#suffix' => '<div class="uw_studentbudget_calculator_otheritems_wrapper">',
  );

  $form['uw_studentbudget_calculator_otherincome_actions_otherdesc'] = array(
    '#type' => 'textfield',
    '#title' => t('Item one'),
    '#size' => 25,
    '#maxlength' => 25,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_description_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherincome_actions_otherdesc', t("Description"), $return_default),
    '#attributes' => array(
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherincome-actions-otherdesc')",
    ),
  );

  $form['uw_studentbudget_calculator_otherincome_actions_othervalue'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 10,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_value_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherincome_actions_othervalue', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherincome-actions-othervalue')",
    ),
  );

  $form['uw_studentbudget_calculator_otherincome_actions_other2desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Item two'),
    '#size' => 25,
    '#maxlength' => 25,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_description_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherincome_actions_other2desc', t("Description"), $return_default),
    '#attributes' => array(
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherincome-actions-other2desc')",
    ),
  );

  $form['uw_studentbudget_calculator_otherincome_actions_other2value'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 10,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_value_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherincome_actions_other2value', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherincome-actions-other2value')",
    ),
  );

  $form['uw_studentbudget_calculator_totalresources_heading'] = array(
    '#prefix' => '</div>',
    '#markup' => t('<h3>Total resources: <span id="uw_studentbudget_calculator_totalresources_calculated_value">$@value</span></h3>', array('@value' => uw_studentbudget_calculator_retreivesession_default('total_resources', '0.00', $return_default))),
  );

  $form['uw_studentbudget_calculator_expenses_heading'] = array(
    '#prefix' => '</div><div class="uw_studentbudget_calculator_mainsection">',
    '#markup' => t('<h2>Your expenses</h2>'),
  );

  $form['uw_studentbudget_calculator_tuition'] = array(
    '#type' => 'textfield',
    '#title' => t('Tuition &amp; incidental fees'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('tuition', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-tuition')",
    ),
  );

  $form['uw_studentbudget_calculator_books'] = array(
    '#type' => 'textfield',
    '#title' => t('Books &amp; supplies'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('books', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-books')",
    ),
  );

  $form['uw_studentbudget_calculator_livingresidence_heading'] = array(
    '#markup' => t('<h3>Living in residence</h3>'),
  );

  $form['uw_studentbudget_calculator_residence'] = array(
    '#type' => 'textfield',
    '#title' => t('Residence'),
    '#field_prefix' => t('Including meal plan'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('residence', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-residence')",
    ),
  );

  $form['uw_studentbudget_calculator_residencephone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('residencephone', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-residencephone')",
    ),
  );

  $form['uw_studentbudget_calculator_livingoffcampus_heading'] = array(
    '#markup' => t('<h3>Living off campus</h3>'),
  );

  $form['uw_studentbudget_calculator_offcampusrent'] = array(
    '#type' => 'textfield',
    '#title' => t('Rent'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('offcampusrent', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-offcampusrent')",
    ),
  );

  $form['uw_studentbudget_calculator_offcampusrent'] = array(
    '#type' => 'textfield',
    '#title' => t('Rent'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('offcampusrent', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-offcampusrent')",
    ),
  );

  $form['uw_studentbudget_calculator_offcampusutilities'] = array(
    '#type' => 'textfield',
    '#title' => t('Utilities'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('offcampusutilities', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-offcampusutilities')",
    ),
  );

  $form['uw_studentbudget_calculator_offcampusphone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('offcampusphone', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-offcampusphone')",
    ),
  );

  $form['uw_studentbudget_calculator_offcampusgroceries'] = array(
    '#type' => 'textfield',
    '#title' => t('Groceries'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('offcampusgroceries', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-offcampusgroceries')",
    ),
  );

  $form['uw_studentbudget_calculator_personal_heading'] = array(
    '#markup' => t('<h3>Personal</h3>'),
  );

  $form['uw_studentbudget_calculator_personalexpenses'] = array(
    '#type' => 'textfield',
    '#title' => t('Personal expenses'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('personalexpenses', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-personalexpenses')",
    ),
  );

  $form['uw_studentbudget_calculator_clothing'] = array(
    '#type' => 'textfield',
    '#title' => t('Clothing'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('clothing', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-clothing')",
    ),
  );

  $form['uw_studentbudget_calculator_personallaundry'] = array(
    '#type' => 'textfield',
    '#title' => t('Laundry'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('personallaundry', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-personallaundry')",
    ),
  );

  $form['uw_studentbudget_calculator_personalent'] = array(
    '#type' => 'textfield',
    '#title' => t('Entertainment'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('personalent', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-personalent')",
    ),
  );

  $form['uw_studentbudget_calculator_transportation_heading'] = array(
    '#markup' => t('<h3>Transportation</h3>'),
  );

  $form['uw_studentbudget_calculator_transportation_local'] = array(
    '#type' => 'textfield',
    '#title' => t('Local transportation'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('transportation_local', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-transportation-local')",
    ),
  );

  $form['uw_studentbudget_calculator_transportation_home'] = array(
    '#type' => 'textfield',
    '#title' => t('Home transportation'),
    '#size' => 6,
    '#maxlength' => 10,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('transportation_home', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-transportation-home')",
    ),
  );

  $form['uw_studentbudget_calculator_otherexpenses_heading'] = array(
    '#markup' => t('<h3>Other expenses</h3>'),
    '#suffix' => '<div class="uw_studentbudget_calculator_otheritems_wrapper">',
  );

  $form['uw_studentbudget_calculator_otherexpenses_actions_otherdesc'] = array(
    '#type' => 'textfield',
    '#title' => t('Item one'),
    '#size' => 25,
    '#maxlength' => 25,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_description_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherexpenses_actions_otherdesc', t("Description"), $return_default),
    '#attributes' => array(
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherexpenses-actions-otherdesc')",
    ),
  );

  $form['uw_studentbudget_calculator_otherexpenses_actions_othervalue'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 10,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_value_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherexpenses_actions_othervalue', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherexpenses-actions-othervalue')",
    ),
  );

  $form['uw_studentbudget_calculator_otherexpenses_actions_other2desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Item two'),
    '#size' => 25,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_description_item'>",
    '#suffix' => "</div>",
    '#maxlength' => 25,
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherexpenses_actions_other2desc', t("Description"), $return_default),
    '#attributes' => array(
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherexpenses-actions-other2desc')",
    ),
  );

  $form['uw_studentbudget_calculator_otherexpenses_actions_other2value'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 10,
    '#prefix' => "<div class='uw_studentbudget_calculator_otheritems_value_item'>",
    '#suffix' => "</div>",
    '#default_value' => uw_studentbudget_calculator_retreivesession_default('otherexpenses_actions_other2value', '0.00', $return_default),
    '#attributes' => array(
      'onblur'  => "uw_studentbudget_calculator.calculate()",
      'onclick'  => "uw_studentbudget_calculator.selectContents('edit-uw-studentbudget-calculator-otherexpenses-actions-other2value')",
    ),
  );

  $form['uw_studentbudget_calculator_totalexpenses_heading'] = array(
    '#prefix' => '</div>',
    '#markup' => t('<h3>Total expenses: <span id="uw_studentbudget_calculator_totalexpenses_calculated_value">$@value</span></h3>', array('@value' => uw_studentbudget_calculator_retreivesession_default('total_expenses', '0.00', $return_default))),
  );

  $form['uw_studentbudget_calculator_netresources_heading'] = array(
    '#suffix' => '</div>',
    '#prefix' => '</div><div class="uw_studentbudget_calculator_mainsection">',
    '#markup' => t('<h2>Net resources: <span id="uw_studentbudget_calculator_netresources_calculated_value">$@value</span></h2>', array('@value' => uw_studentbudget_calculator_retreivesession_default('net_total', '0.00', $return_default))),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Calculate'),
  );

  $form['reset'] = array(
    '#type' => 'button',
    '#value' => t('Reset'),
    '#attributes' => array('onclick' => 'this.form.reset(); uw_studentbudget_calculator.reset();return false;'),
  );

  return $form;
}

/**
 * Display the budget calculator form.
 */
function uw_studentbudget_calculator_displayform($form, &$form_state) {
  return uw_studentbudget_calculator_generateform($form);
}

/**
 * Gets the item from the form field and converts to a float, as well as setting the session variable with the converted value.
 *
 * @param array $form_state
 *   Form state array.
 * @param string $id
 *   ID of the form piece.
 *
 * @return float
 *   The item value.
 */
function uw_studentbudget_calculator_helper_get_dollar_value(&$form_state, $id) {
  $form_value_id = 'uw_studentbudget_calculator_' . $id;

  $converted_float = floatval($form_state['values'][$form_value_id]);
  uw_studentbudget_calculator_data_store($id, number_format($converted_float, 2, '.', ''));

  return $converted_float;
}

/**
 * Store the supplied value in the session data store.
 */
function uw_studentbudget_calculator_data_store($id, $value) {
  $_SESSION['uw_studentbudget_calculator_data'][$id] = $value;
}

/**
 * Gets the two values (amount and month) and calculates them to create (total), places in appropriate sessions, and stores.
 */
function uw_studentbudget_calculator_helper_get_calculated_value(&$form_state, $total_id, $amount_id, $month_id) {
  $calculated_value = uw_studentbudget_calculator_helper_get_dollar_value($form_state, $amount_id) * uw_studentbudget_calculator_helper_get_dollar_value($form_state, $month_id);
  uw_studentbudget_calculator_data_store($total_id, number_format($calculated_value, 2, '.', ''));
  return $calculated_value;
}

/**
 *
 */
function uw_studentbudget_calculator_displayform_submit($form, &$form_state) {
  // Create the data store if it does not exist yet.
  if (!isset($_SESSION['uw_studentbudget_calculator_data'])) {
    $_SESSION['uw_studentbudget_calculator_data'] = array();
  }

  // Store study period option checked.
  uw_studentbudget_calculator_data_store('study_period', $form_state['values']['uw_studentbudget_calculator_study_period']);

  // Calculate.
  $total_resources = uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'beginbalance')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'parents')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'awards')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'parttime')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'government_orphan')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'government_welfare')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'osap_canada')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'otherincome_actions_othervalue')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'otherincome_actions_other2value');

  uw_studentbudget_calculator_data_store('otherincome_actions_otherdesc', $form_state['values']['uw_studentbudget_calculator_otherincome_actions_otherdesc']);
  uw_studentbudget_calculator_data_store('otherincome_actions_other2desc', $form_state['values']['uw_studentbudget_calculator_otherincome_actions_other2desc']);

  uw_studentbudget_calculator_data_store('total_resources', number_format($total_resources, 2, '.', ''));

  $total_expenses = uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'tuition')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'books')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'residence')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'residencephone')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'offcampusrent')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'offcampusutilities')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'offcampusphone')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'offcampusgroceries')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'personalexpenses')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'clothing')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'personallaundry')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'personalent')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'transportation_local')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'transportation_home')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'otherexpenses_actions_othervalue')
    + uw_studentbudget_calculator_helper_get_dollar_value($form_state, 'otherexpenses_actions_other2value');

  uw_studentbudget_calculator_data_store('otherexpenses_actions_otherdesc', $form_state['values']['uw_studentbudget_calculator_otherexpenses_actions_otherdesc']);
  uw_studentbudget_calculator_data_store('otherexpenses_actions_other2desc', $form_state['values']['uw_studentbudget_calculator_otherexpenses_actions_other2desc']);

  uw_studentbudget_calculator_data_store('total_expenses', number_format($total_expenses, 2, '.', ''));

  $net_total = $total_resources - $total_expenses;

  uw_studentbudget_calculator_data_store('net_total', number_format($net_total, 2, '.', ''));

  $form_state['redirect'] = array("uw_studentbudget_calculator", array("query" => array("c" => "1")));
}
